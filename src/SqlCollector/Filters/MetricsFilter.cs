﻿using System.Text.RegularExpressions;
using Microsoft.Extensions.Options;

namespace SqlCollector.Sources;

public class MetricsFilter
{
    private readonly MetricsFilterOptions _options;

    public MetricsFilter(IOptions<MetricsFilterOptions> options)
    {
        _options = options.Value;
    }

    public bool Matches(MetricValue value)
    {
        if (_options.IgnoredMetrics?.Any() is not true)
            return true;

        int? indexOfLabel = null;

        foreach (var ignoredMetric in _options.IgnoredMetrics)
        {
            if (!Regex.IsMatch(value.MetricName, ignoredMetric.MetricName))
                continue;

            indexOfLabel ??= (Array.IndexOf(value.Labels, ignoredMetric.LabelName));

            if (indexOfLabel < 0)
                continue;

            if (ignoredMetric.Values is null)
                throw new InvalidOperationException("Ignored metrics should contain at least one value to match");

            if (ignoredMetric.Values.Contains(value.LabelValues[indexOfLabel.Value]))
                return false;
        }

        return true;
    }
}