﻿namespace SqlCollector.Sources;

public class MetricsFilterOptions
{
    public const string MetricsFilter = nameof(MetricsFilter);
    public IgnoredMetric[]? IgnoredMetrics { get; set; }
}

public class IgnoredMetric
{
    public string MetricName { get; set; }
    public string LabelName { get; set; }
    public string[]? Values { get; set; }
}