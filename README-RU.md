# SQL Server Monitoring
Решение для мониторинга производительности Microsoft SQL Server. Оно не требует установки каких-либо компонент на 
машину, где запущен SQL Server, а осуществляет удалённый мониторинг.
В нём используется Prometheus как коллектор метрик, Grafana и преднастроенные дашборды как решение для визуализации ключевых показателей.

![Dashboard by databases](./docs/dashboard-databases.png "Databases KPI")
![Dashboard overview](./docs/dashboard-overview.png "Resources overview")

## Установка

Склонируйте этот репозиторий на **любой** сервер где установлен Docker. Перейдите в папку `config` и скопируйте файл `sql-collector.example.yaml` в `sql-collector.yaml`.
Отредактируйте в `sql-collector.yaml` строку подключения к вашему SQL-серверу.
Затем просто запустите контейнеры:
```sh
docker compose up -d
```

## Доступ к дашбордам
Графана доступна по адресу http://localhost:3000/. Используйте логин `admin` и пароль `monitoring`.
Если вдруг кому-то понадобится, Prometheus UI доступен по адресу http://localhost:9090/

## Настройка
Самые полезные группы счётчиков производительности включены в конфигурацию по-умолчанию (см. `appsettings.yaml`, секция `SqlMonitoring:Groups`):
- SQLServer:Access Methods
- SQLServer:Buffer Manager
- SQLServer:Databases
- SQLServer:Deprecated Features
- SQLServer:General Statistics
- SQLServer:Latches
- SQLServer:Locks
- SQLServer:Memory Manager
- SQLServer:Plan Cache
- SQLServer:Resource Pool Stats
- SQLServer:SQL Statistics
- SQLServer:Transactions
- SQLServer:Wait Statistics
  
Набор всех доступных метрик описан в статье https://learn.microsoft.com/en-us/sql/relational-databases/performance-monitor/use-sql-server-objects?view=sql-server-ver16