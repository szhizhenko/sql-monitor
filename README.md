# SQL Server Monitoring
This is ready to use solution consisting of Prometheus metrics collector and Grafana with dashboards to monitor key metrics in MS SQL Server.
This solution does not requires installing anything on SQL host - it can be used as remote monitoring.

![Dashboard by databases](./docs/dashboard-databases.png "Databases KPI")
![Dashboard overview](./docs/dashboard-overview.png "Resources overview")

## Installation

Clone this repository to any server with Docker installed. Goto `config` folder and make copy of `sql-collector.example.yaml` to `sql-collector.yaml`.
Edit SQL server connection string and point it to your SQL server.
Then just run containers:
```sh
docker compose up -d
```

## Accessing to dashboards
Open your browser with URL http://localhost:3000/. Use 'admin' and 'monitoring' to login into Grafana.
Prometheus UI is available at http://localhost:9090/


## Configuration
Most useful counter groups are included in default configuration (see `appsettings.yaml` `SqlMonitoring:Groups` section):
- SQLServer:Access Methods
- SQLServer:Buffer Manager
- SQLServer:Databases
- SQLServer:Deprecated Features
- SQLServer:General Statistics
- SQLServer:Latches
- SQLServer:Locks
- SQLServer:Memory Manager
- SQLServer:Plan Cache
- SQLServer:Resource Pool Stats
- SQLServer:SQL Statistics
- SQLServer:Transactions
- SQLServer:Wait Statistics

All counters are described at https://learn.microsoft.com/en-us/sql/relational-databases/performance-monitor/use-sql-server-objects?view=sql-server-ver16